import com.sun.jna.Library;
import com.sun.jna.Native;

public interface JnaDemo extends Library {
    JnaDemo INSTANCE = Native.load("CoreDumpTest", JnaDemo.class);

    int clib_run();
}
