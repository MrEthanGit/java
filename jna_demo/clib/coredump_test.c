#include <stdio.h>
#include <string.h>

int clib_coredump_test(const char *src, int len)
{
//    char *dst = NULL;
    char dst[32] = {0};
	
    printf("enter clib_coredump_test, src:%s len:%d\n", src, len);
    strncpy(dst, src, len);
    return 0;
}

int clib_run()
{
    printf("clib is start\n");
    const char *str = "coredump test";
    int ret = clib_coredump_test(str, strlen(str));
    printf("clib_run ret:%d\n", ret);

    return 0;
}
